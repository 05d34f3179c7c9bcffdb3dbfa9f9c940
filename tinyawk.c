#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
int main(int argc, char *argv[]) {

  if (argc < 2) return 1;

  // awk '    {     print      $    3      }     '
  char *ptr = argv[1], *tmp;
#define skipspace(x) while (*x == ' ' || *x == '\t') x++;
  skipspace(ptr);
  if (*ptr++ != '{') return 1;
  skipspace(ptr);
  if (strncmp(ptr, "print", 5)) return 1;
  ptr += 5;
  skipspace(ptr);
  if (*ptr++ != '$') return 1;
  skipspace(ptr);
  long field = strtol(ptr, &tmp, 10);
  ptr = tmp;
  skipspace(ptr);
  if (*ptr++ != '}') return 1;
  skipspace(ptr);
  if (*ptr) return 1;

  ssize_t read;
  size_t len = 0;
  char *line = NULL;
  FILE *f = stdin;
  argv++;
  if (argc == 2) goto inner;
  while (*++argv) {
    if (!strcmp(*argv, "-")) f = stdin;
    else if (!(f = fopen(*argv, "r"))) return 1;

inner:
    while ((read = getline(&line, &len, f)) > 0) {
      if (field == 0) { fwrite(line, 1, read, stdout); continue; }
      if (line[read-1] == '\n') line[read-1] = 0;
      skipspace(line);
      for (long i = 1; i < field; i++) {
        while (*line != ' ' && *line != '\t') line++;
        skipspace(line);
      }
      if ((tmp = strchr(line, ' '))) tmp[0] = 0;
      if ((tmp = strchr(line, '\t'))) tmp[0] = 0;
      puts(line);
    }
    if (f != stdin) fclose(f);
  }
  return 0;
}
